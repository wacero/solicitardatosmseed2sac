# Información básica 

Estos son los scripts creados para que las personas de monitoreo de IGEPN puedan 
conectarse al servidor  ARCLINK, convertir datos a SAC y usar estos datos en el programa SIPAS.


# Instalación

Ejecutar dentro de un entorno virtual Conda

Descargar miniconda

Crear el entorno e instalar obspy
``` bash 
$ conda create -n mseed2sac python
$ conda activate mseed2sac
$ conda config --add channels conda-forge
$ conda install obspy
$ pip install get_mseed_data
```


# Ejecución

```bash
$ conda activate mseed2sac
$ python ./run_solicitar_datos.py ./archivo_configuracion.txt
``` 

# Documentación

La documentación del proyecto se encuentra en la siguiente URL: 
https://wacero.gitlab.io/solicitardatosmseed2sac/

# CI/CD

No se incluyó la generación de documentación ni las pruebas con PYTEST en el archivo .gitlab-ci.yml debido a que la instalación de las librerías toma demasiado tiempo. Esto se podría solucionar creando un docker con las librerías necesarias, pero aún no se conoce bien como hacerlo. 
