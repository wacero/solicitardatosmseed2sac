'''
Created on 31/03/2015

@author: wacero
'''

import os,sys
import logging
import solicitar_datos
from get_mseed_data import get_mseed
from get_mseed_data import get_mseed_utils as gmutils

"""
LEER EL ARCHIVO DE CONFIGURACION:
    Cargar el archivo de  estaciones json
    cargar el archivo de volcanes json
    Leer el volcan y fecha en el archivo de configuracion 
    
    Extraer los datos para ese volcan y esa estacion 
    
"""

def main():
    """
    This script read the configuration text file, set value for the parameters and execute the request modules
    """
    
    exec_dir=os.path.realpath(os.path.dirname(__file__))
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',filename=os.path.join(exec_dir,'solicitar_datos_log.txt'),level=logging.INFO)
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True

    else:
        try:
            run_parameters=gmutils.read_parameters(sys.argv[1])
        
        except Exception as e:
            logging.error("Error reading configuration sets in file: %s" %str(e))
            raise Exception("Error reading configuration sets in file: %s" %str(e))
        
        try:
            mseed_server_type=run_parameters['MSEED_SERVER']['type']
            mseed_server_config_file=run_parameters['MSEED_SERVER']['file_path']
            stations_file=run_parameters['STATION_FILE']['file_path']
            volcanoes_file=run_parameters['VOLCANO_FILE']['file_path']
            
            request_volcano=run_parameters['REQUEST']['volcano']
            request_date= run_parameters['REQUEST']['date']
            cut_size_minutes = int(run_parameters['REQUEST']['cut_size_minutes'])
        
        except Exception as e:
            logging.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))
                
        try:
            mseed_server_parameters=gmutils.read_config_file(mseed_server_config_file)
            mseed_server_dict=mseed_server_parameters[mseed_server_type]
            mseed_client=get_mseed.choose_service(mseed_server_dict)
        
        except Exception as e:
            logging.error("Error getting mseed client: %s" %str(e))
            raise Exception("Error getting mseed_client: %s" %str(e))
        
        try:  
            stations_dict=gmutils.read_config_file(stations_file)
            volcanoes_dict=gmutils.read_config_file(volcanoes_file)
            station_list=volcanoes_dict[request_volcano][request_volcano]           
        
        except Exception as e:
            logging.error("Error reading station and volcano file: %s" %str(e))
            raise Exception("Error reading station and volcano file: %s" %str(e))
        
        try:
            filter_enable=int(run_parameters['FILTER']['enable'])
            filter_dict={"name":run_parameters['FILTER']['name'], "freq_1":float(run_parameters['FILTER']['freq_1']), 
                         "freq_2":float(run_parameters['FILTER']['freq_2'])
                         }
        
        except Exception as e:
            logging.error("Error reading filter parameters")
            raise Exception("Error reading filter parameters")
        
        try:
            station_dict_list=[]
            for station in station_list:
                station_dict_list.append(stations_dict[station])
                   
            solicitar_datos.data_request(request_volcano, station_dict_list, request_date,mseed_server_type,
                                            mseed_client,filter_enable,filter_dict,cut_size_minutes)
        except Exception as e:
            logging.error("Error in data_request. %s" %str(e))
            raise Exception("Error in data_request. %s" %str(e))
            

    if is_error:
        logging.info(f': python {sys.argv[0]} archivo_configuracion.txt ')
        print(f'USO: python {sys.argv[0]} archivo_configuracion.txt ')    


if __name__ == "__main__":
    main()

      

