'''
Created on 12/03/2015

@author: wacero

'''
import os
import logging

from obspy.core import UTCDateTime
from obspy.core import stream 
from obspy.signal import filter
import numpy as np 

from get_mseed_data import get_mseed

data_dir="./SAC"
SECONDS_IN_DAY=86400

def date_string2UTCDateTime(date_string):
    """ 
    Convierte la fecha string en un objeto UTCDateTime 
    
    :param string date_string: fecha en formato string
    :returns: obspy.core.UTCDateTime
    """
    
    try:
        return UTCDateTime(date_string,precision=0)
    
    except Exception as e:
        raise Exception("Error in date_string2UTCDateTime() : %s " %str(e))


def write_sliced_data(sliced_stream,sac_data_dir):
    """
    Escribe los datos dividos de una estacion en directorios
    
    :param list sliced_stream: lista de objetos stream
    :param string sac_data_dir: ruta del directorio 
    """
    file_name="%s.%s.%s" %(sliced_stream[0][0].stats['station'],sliced_stream[0][0].stats['channel'],sliced_stream[0][0].stats['network'])
    for i in range(0,len(sliced_stream)):
        start_time=sliced_stream[i][0].stats['starttime']
        start_time_str=start_time.strftime("%d%H%M")
        start_time_str="%s00" %start_time_str
        
        try:
            sliced_stream[i].write("%s/%s/%s.sac" %(sac_data_dir,start_time_str,file_name), format="SAC")
        except Exception as e:
            logging.error("Error en write_sliced_data(args) : " + str(e))
        

def slice_stream(stream_mseed,request_datetime,window_size):
    """
    Recibe los datos de una estacion de un dia de duracion y los divide de acuerdo al periodo especificado
    
    :param list stream_mseed: datos a dividir 
    :param obspy.UTCDateTime request_datetime: datetime del inicio de los datos
    :param int periodo: numero por el cual seran divididos los datos
    :returns: stream_divided: lista con objetos stream
    """
    stream_divided=[]
    #Trick para corregir error de precision
    request_datetime=request_datetime + 0.01
    for x in range (0,int(SECONDS_IN_DAY/window_size)):

        start_time=request_datetime + (window_size*x)
        end_time=request_datetime + (window_size*(x+1))     
        temp_stream = stream_mseed.slice(start_time , end_time)

        if len(temp_stream)==0:
            print("Gap en datos")
        else:          
            stream_divided.insert(x,temp_stream)    
    
    return stream_divided

def filter_streams(stream_list,filter_dict):
    """ 
    Filtra una lista de datos mediante bandpass o highpass
    
    :param list stream_list: lista de objetos stream
    :param dict filter_dict: diccionario con informacion del filtro 
    :returns: lista de datos filtrados 
    """
    
    
    if filter_dict['name'] == "bandpass":
        print("Start bandpass filter")
        for stream in stream_list:
            stream.detrend('linear') 
            data=filter.bandpass(stream[0].data, filter_dict["freq_1"],filter_dict["freq_2"], stream[0].stats['sampling_rate'], 4)
            stream[0].data=data
                        
        return stream_list
    
    elif filter_dict["name"] == "highpass":
        print("Start highpass filter")
        for stream in stream_list:
            stream.detrend('linear')
            data=filter.highpass(stream[0].data, filter_dict["freq_1"], stream[0].stats['sampling_rate'], 4)
            stream[0].data=data                
        return stream_list

def create_folder(folder_path):
    """ 
    Crea una carpeta si no existe
    
    :param string folder_path: path de la carpeta a crear
    """
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

def create_subfolder(data_dir,dia,cut_size_minutes):
    """ 
    Crea carpetas acorde el dia y la hora
    
    :param string data_dir: directorio de los datos 
    :param string dia: dia del mes
    :param int cut_size_minutes: tama��o de corte en minutos 
    """
    for i in range(0,24):
        hora="%02d" %i
        for min_interval in np.arange(0,60,cut_size_minutes):
            create_folder("%s/%s%s%02d00"%(data_dir,dia,hora,min_interval))





def data_request(volcano,station_list,request_date,mseed_server_type,mseed_client,filter_enable,filter_dict,cut_size_minutes):
    """
    solicita datos y se encarga de llamar al resto de funciones
    
    :param string volcano: volcan
    :param list station list: lista de estaciones
    :param string request_date: fecha en formato "YYYY-mm-dd"
    :param string mseed_server_type: tipo de servidor 
    :param string mseed_client: cliente
    :param string filter_enable: datos filtrados 
    :param string filter_dict: filtra los datos en diccionarios
    :param string cut_size_minutes): tamano de corte en minutos 
    """
    cut_size_seconds= int(cut_size_minutes*60) 
    request_date=date_string2UTCDateTime(request_date)
    sac_data_dir="%s/%s/%s%s" %(data_dir,volcano,request_date.year,request_date.month)
    
    create_folder(sac_data_dir)
    #create_subfolder(sac_data_dir, "%02d" %request_date.day)
    create_subfolder(sac_data_dir, "%02d" %request_date.day,cut_size_minutes)
    for station in station_list:
        for loc in station['loc']:
            for cha in station['cha']:
                print(station['net'],station['cod'],loc,cha,request_date ,86400)
                mseed_stream=get_mseed.get_stream(mseed_server_type,mseed_client,station['net'],station['cod'],loc,cha,start_time=request_date,window_size=86400)

                if mseed_stream != None:
                    if len(mseed_stream)>0:
                        print("OK stream:%s" %mseed_stream)
                        #mseed_stream.merge(method=0,fill_value='latest')
                        #gviracucha decidio utilizar el metodo interpolate
                        mseed_stream.merge(method=1,fill_value='interpolate')
                        sliced_stream=slice_stream(mseed_stream,request_date,cut_size_seconds)
                        if filter_enable==1 and cha != 'SHZ' and cha != 'BDF' :  
                            print("Filter data")                           
                            write_sliced_data(filter_streams(sliced_stream,filter_dict),sac_data_dir)                            
                        else:                        
                            write_sliced_data(sliced_stream,sac_data_dir)
                    else:
                        print("Fail to get stream")
                        