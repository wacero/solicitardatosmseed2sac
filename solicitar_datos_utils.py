'''
Created on Jan 3, 2020

@author: wacero
'''
import json
import logging
import configparser


def read_config_json_file(json_file_path):
    """
    Lee un archivo json_file y lo devuelve como un diccionario de Python
    
    :param string json_file_path: ruta a un archivo json con información de configuración
    :returns: dict
    """
    
    json_file=check_file(json_file_path)
    with open(json_file) as json_data:
        return json.load(json_data)


def read_parameters(file_path):
    """
    Lee un archivo de texto de configuración
    
    :param string file_path: ruta al archivo de texto de configuración
    :returns: dict: dict of a parser object
    """

    parameter_file=check_file(file_path)
    parser=configparser.ConfigParser()
    parser.read(parameter_file)
    return parser._sections

def check_file(file_path):
    """
    Comprueba si el archivo existe
    
    :param string file_path: ruta al archivo para verificar
    :return: file_path
    :raises Exception e: Excepción general si el archivo no existe.
    """
    try:
        with open(file_path):
            return file_path
    except Exception as e:
        logging.error("Error in check_file(%s). Error: %s " %(file_path,str(e)))
        raise Exception("Error in check_file(%s). Error: %s " %(file_path,str(e)))
