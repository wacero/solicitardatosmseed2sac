import pytest 
import solicitar_datos
from obspy.core import UTCDateTime
from obspy.core import read
import os 

def test_date_string2UTCDateTime():
    date_time_string="2004-05-24 00:00:00"
    date_time_expected = UTCDateTime(date_time_string)
    
    date_time_tested=solicitar_datos.date_string2UTCDateTime(date_time_string)
    
    assert date_time_string == date_time_expected
   
def test_write_sac():
    mseed_file_path="./test/data/EC.ANTS..HHZ.D.2016.107"
    stream_expected = read(mseed_file_path)
    stream_expected.write("./test/data/ants_expected.sac", format="SAC")
    solicitar_datos.write_sac(stream_expected, "ants_tested.sac","./data/")
    
    assert read("./test/data/ants_expected.sac") == read("./test/data/ants_expected.sac")
    
def test_create_folder():
    folder_path="./test/data/prueba"
    folder_string_expected = os.makedirs(folder_path)
    folder_string_tested=solicitar_datos.create_folder(folder_path)
    #assert folder_path == folder_string_expected  
    assert os.path.exists(folder_path)

    
    
