import pytest 

import sys
import json
#sys.path.append('../')

import solicitar_datos_utils

def test_read_config_json_file():

    station_data_expected="""{
            "ANGU":{        "net":"EC",     "cod":"ANGU",   "loc":[""],     "cha":["HHZ"]},
            "BMAS":{        "net":"EC",     "cod":"BMAS",   "loc":[""],     "cha":["BHZ"]},
            "BNAS":{        "net":"EC",     "cod":"BNAS",   "loc":[""],     "cha":["BHZ"]}
            }"""
    station_data_expected_dict= json.loads(station_data_expected)
     
    station_data_test=solicitar_datos_utils.read_config_json_file("./config/stations_EXAMPLE.json")
    
    assert station_data_test == station_data_expected_dict
    
    
    
